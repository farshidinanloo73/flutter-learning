import 'package:go_router/go_router.dart';
import 'package:practice_flutter/Pages/HomePage/view/index.dart';
import 'package:practice_flutter/Pages/TestPage/index.dart';

final appRouter = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const HomePage(),
    ),
    GoRoute(
      path: '/test',
      builder: (context, state) => const TestPage(),
    ),
  ],
);
