import 'dart:convert';

class Home {
  final int id;
  final String title;
  final int userId;

  Home({required this.id, required this.title, required this.userId});

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Home && o.id == id && o.title == title;
  }

  @override
  int get hashCode => id.hashCode ^ title.hashCode;

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    result.addAll({'id': id});
    result.addAll({'title': title});
    result.addAll({'userId': userId});

    return result;
  }

  factory Home.fromMap(Map<String, dynamic> map) {
    return Home(
      id: map['id']?.toInt() ?? 0,
      title: map['title'] ?? '',
      userId: map['userId'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Home.fromJson(String source) => Home.fromMap(json.decode(source));
}
