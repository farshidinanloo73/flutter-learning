import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_flutter/Pages/HomePage/cubit/home_cubit.dart';
import 'package:practice_flutter/di/dependency_injection.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => injector<HomeCubit>(),
        ),
      ],
      child: Scaffold(
        body: Center(
          child: BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state) {
              if (state is HomeLoading) {
                return const CircularProgressIndicator();
              } else if (state is HomeLoaded) {
                return Text(state.home.title);
              } else if (state is HomeError) {
                return Text(state.message);
              } else {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      "home page",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 10),
                    ElevatedButton(
                      onPressed: () {
                        final homeCubit = BlocProvider.of<HomeCubit>(context);
                        homeCubit.getHome('1');
                        // GoRouter.of(context).go('/test');
                      },
                      child: const Text("fetch home"),
                    ),
                  ],
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
