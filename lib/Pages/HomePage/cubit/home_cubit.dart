import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:practice_flutter/Pages/HomePage/models/index.dart';
import 'package:practice_flutter/Pages/HomePage/repository/index.dart';

part 'home_state.dart';

@injectable
class HomeCubit extends Cubit<HomeState> {
  final FakeHomeRepository _homeRepository;

  HomeCubit(this._homeRepository) : super(HomeInitial());

  Future<void> getHome(String id) async {
    try {
      emit(HomeLoading());
      final home = await _homeRepository.fetchHome(id);
      emit(HomeLoaded(home));
    } catch (e) {
      emit(HomeError(e.toString()));
    }
  }
}
