import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:practice_flutter/Config/network/dio_client.dart';
import 'package:practice_flutter/Pages/HomePage/models/index.dart';
import 'package:practice_flutter/utils/device/dio/dio_error_util.dart';

@injectable
class FakeHomeRepository {
  final DioInstance dioClint;
  FakeHomeRepository(this.dioClint);

  Future<Home> fetchHome(String id) async {
    try {
      var response = await dioClint.get('/albums/$id');
      return Home.fromMap(response);
    } on DioError catch (e) {
      throw DioErrorUtil.handleError(e);
    } catch (e) {
      rethrow;
    }
  }
}

class NetworkException implements Exception {}
