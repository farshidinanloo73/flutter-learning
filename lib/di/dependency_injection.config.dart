// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i6;

import '../Config/network/dio_client.dart' as _i3;
import '../Config/sharedpref/shared_preference_helper.dart' as _i7;
import '../Pages/HomePage/cubit/home_cubit.dart' as _i5;
import '../Pages/HomePage/repository/index.dart' as _i4;
import 'dependency_injection.dart'
    as _i8; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final injectableModule = _$InjectableModule();
  gh.factory<_i3.DioInstance>(() => _i3.DioInstance());
  gh.factory<_i4.FakeHomeRepository>(
      () => _i4.FakeHomeRepository(get<_i3.DioInstance>()));
  gh.factory<_i5.HomeCubit>(() => _i5.HomeCubit(get<_i4.FakeHomeRepository>()));
  await gh.factoryAsync<_i6.SharedPreferences>(() => injectableModule.prefs,
      preResolve: true);
  gh.factory<_i7.SharedPreferenceHelper>(
      () => _i7.SharedPreferenceHelper(get<_i6.SharedPreferences>()));
  return get;
}

class _$InjectableModule extends _i8.InjectableModule {}
