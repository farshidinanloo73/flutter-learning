import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dependency_injection.config.dart';


final injector  = GetIt.instance;

@module
abstract class InjectableModule{

 @preResolve
 Future<SharedPreferences> get prefs => SharedPreferences.getInstance();
 
}

@InjectableInit(
  initializerName: r'$initGetIt', // default
  preferRelativeImports: true, // default
  asExtension: false, // default
)
Future<void> configureInjection(String env) async {
 await $initGetIt(injector, environment: env);
}


